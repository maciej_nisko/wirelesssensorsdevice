/*
 * buttons.c
 *
 *  Created on: 13 sty 2015
 *      Author: maciej
 */

#include "buttons.h"
#include <avr/io.h>
#include <util/delay.h>


#define DEBOUNCE_DELAY	50


/*
 * if pressed return 1, otherwise return 0
 */
uint8_t PB_CheckIfPressed(PB_Button_t b) {
	if( !(PB_PIN & (1 << b)) ) {
		/* insert small delay to wait for the state
		 * of the button stabilize */
		_delay_ms(DEBOUNCE_DELAY);
		/* check button state once again */
		if( !(PB_PIN & (1 << b)) ) {
			return 1;
		}
	}
	return 0;
}


void PB_InitButtons() {
	/* PC2 and PC3 as inputs */
	PB_DDR &= ~(1 << PB_SEL);
	PB_DDR &= ~(1 << PB_SET);

	/* enable internal pull-up resistors */
	PB_PORT |= (1 << PB_SEL);
	PB_PORT |= (1 << PB_SET);
}
