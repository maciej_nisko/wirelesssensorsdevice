/*
 * buttons.h
 *
 *  Created on: 13 sty 2015
 *      Author: maciej
 */

#ifndef BUTTONS_H_
#define BUTTONS_H_


#include <avr/io.h>


/* push button SEL connected to pin PC2
 * HIGH when released, LOW when pushed*/
#define PB_PORT			PORTC
#define PB_PIN			PINC
#define PB_DDR			DDRC
#define PB_SEL			2
#define PB_SET			3


typedef enum {
	BUTTON_SEL = 2,
	BUTTON_SET = 3
} PB_Button_t;

/* check if button is pressed */
uint8_t PB_CheckIfPressed(PB_Button_t b);

/* set buttons' registers */
void PB_InitButtons();


#endif /* BUTTONS_H_ */
