/*
 * fsm.c
 *
 *  Created on: 12 sty 2015
 *      Author: maciej
 */

#include "fsm.h"
#include "hd44780.h"
#include "buttons.h"
#include "one_wire.h"


#define LCD_DELAY	50


/*
 * A table that consists of function pointers. Each
 * function is treated as a different state the FSM
 * can be in.
 */
state_func_t* const state_table[NUM_STATES] = {
		InStateTimerset,
		InStateTempset,
		InStateDisplay
};

/*
 * state description:
 * a state in which you can change the clock hour
 * when button pressed - go to minute settings
 * when button pressed again - go to ST_TEMPSET
 * clock blinking
 *
 */
state_t InStateTimerset(instance_data_t *data) {

	/* prevent from refreshing the LCD screen if given
	 * state is already ST_TIMERSET */
	if(data->cur_state != ST_TIMERSET) {
		LCD_Clear();
		LCD_GoTo(0, 0);
		LCD_WriteText("InStateTimerset");
	}

	/* insert delay so LCD can update value */
	_delay_ms(LCD_DELAY);

	/* if button is pressed, change state to ST_TEMPSET */
	if(PB_CheckIfPressed(PB_SEL)) {
		/* set dummy state to make switching between
		 * states look smooth */
		data->cur_state = NUM_STATES;
		return ST_TEMPSET;
	}

	/* pass current state to the global structure */
	data->cur_state = ST_TIMERSET;

	return ST_TIMERSET;
}

/*
 * State description:
 * in this state one can set a temperature at which
 * alarm should go off.
 */
state_t InStateTempset(instance_data_t *data) {

	/* prevent from refreshing the LCD screen if given
	 * state is already ST_TEMPSET */
	if(data->cur_state != ST_TEMPSET) {
		LCD_Clear();
		LCD_GoTo(0, 0);
		LCD_WriteText("InStateTempset");
	}

	/* insert delay so LCD can update value */
	_delay_ms(LCD_DELAY);

	/* if button is pressed, change state to ST_DISPLAY */
	if(PB_CheckIfPressed(PB_SEL)) {
		/* set dummy state to make switching between
		 * states look smooth */
		data->cur_state = NUM_STATES;
		return ST_DISPLAY;
	}

	/* pass current state to the global structure */
	data->cur_state = ST_TEMPSET;

	return ST_TEMPSET;
}

/*
 * state description:
 * application initial state
 * when button pressed - go to ST_TIMERSET
 * display all the features on the screen
 */
state_t InStateDisplay(instance_data_t *data) {

	/* prevent from refreshing the LCD screen if given
	 * state is already ST_DISPLAY */
	if(data->cur_state != ST_DISPLAY) {

		/* measure temperature */
		char tempBuffer[8];
		OW_GetTemp(tempBuffer);

		LCD_Clear();
		DisplayMainScreen("time", tempBuffer, "light", "humid");
	}

	/* insert delay so LCD can update value */
	_delay_ms(LCD_DELAY);

	/* if button is pressed, change state to ST_TIMERSET */
	if(PB_CheckIfPressed(PB_SEL)) {
		/* set dummy state to make switching between
		 * states look smooth */
		data->cur_state = NUM_STATES;
		return ST_TIMERSET;
	}

	/* pass current state to the global structure */
	data->cur_state = ST_DISPLAY;

	return ST_DISPLAY;
}

/*
 * The main function of the state machine. It takes two arguments:
 * a state to be executed and an address to data structure that
 * can be passed between different states. Returns the new state.
 */
state_t RunState(state_t cur_state, instance_data_t *data) {
	return state_table[cur_state](data);
}


/*
 * Functions to operate the LCD
 */
void DisplayMainScreen(char* a, char *b, char* c, char* d) {
	LCD_GoTo(0, 0);
	LCD_WriteText(a);

	LCD_GoTo(8, 0);
	LCD_WriteText(b);

	LCD_GoTo(0, 1);
	LCD_WriteText(c);

	LCD_GoTo(8, 1);
	LCD_WriteText(d);
}
