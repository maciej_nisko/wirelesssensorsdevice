/*
 * fsm.h
 *
 *  Created on: 12 sty 2015
 *      Author: maciej
 */

#ifndef FSM_H_
#define FSM_H_



/*
 * define application behavior with states
 */
typedef enum {
	ST_TIMERSET,
	ST_TEMPSET,
	ST_DISPLAY,
	NUM_STATES
} state_t;


/*
 * structure that holds and passes data between states
 */
typedef struct {
	state_t cur_state;
} instance_data_t;


/*
 * typedef StateFunc_t for a function taking instanceData_t as an argument
 * and returning state_t
 */
typedef state_t state_func_t(instance_data_t *data);


/*
 * table for storing pointers to every state functions
 */
state_func_t* const state_table[NUM_STATES];


/*
 * define the behavior of the application in each state
 * each function should contain a set of conditions allowing to
 * transition to next state(s) if the conditions are met.
 * returning next state
 */
state_t InStateTimerset(instance_data_t *data);
state_t InStateTempset(instance_data_t *data);
state_t InStateDisplay(instance_data_t *data);


/*
 * this function is responsible for running and changing states
 * it takes the current state and data as arguments and returns
 * a new state
 */
state_t RunState(state_t curState, instance_data_t *data);


/*
 * Other useful functions to handle the LCD
 */
void DisplayMainScreen(char*, char*, char*, char*);

#endif /* FSM_H_ */
