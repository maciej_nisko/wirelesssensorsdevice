/*
 * led_digit.c
 *
 *  Created on: 15 lip 2014
 *      Author: maciej
 */

#include "led_digit.h"


/*
 *	show the digit on the display
 */
void displayPrintDigit(leddigit_t digit) {
	DISPLAY_DIGIT_PORT = digit;
}

/*
 *	light the whole display
 */
void displayOn() {
	DISPLAY_DIGIT_PORT = 0b10000000;
}

/*
 *	turn off the whole display
 */
void displayOff() {
	DISPLAY_DIGIT_PORT = 0b11111111;
}

/*
 *	increment the value on a display by one
 */
leddigit_t displayIncr() {
	// led display port as input
	DISPLAY_DIGIT_DDR = 0x00;
	DISPLAY_DIGIT_PORT = 0x00;

	// get the current displayed value
	leddigit_t current = 0x00;
	if (PIND & (1<<PD0)) current |= 0x01;
	if (PIND & (1<<PD1)) current |= 0x02;
	if (PIND & (1<<PD2)) current |= 0x04;
	if (PIND & (1<<PD3)) current |= 0x08;
	if (PIND & (1<<PD4)) current |= 0x10;
	if (PIND & (1<<PD5)) current |= 0x20;
	if (PIND & (1<<PD6)) current |= 0x40;
	if (PIND & (1<<PD7)) current |= 0x80;

	// set back port as output
	DISPLAY_DIGIT_DDR = 0xff;
	DISPLAY_DIGIT_PORT = 0xff;

	if (current == LED_0) return LED_1;
	else if (current == LED_1) return LED_2;
	else if (current == LED_2) return LED_3;
	else if (current == LED_3) return LED_4;
	else if (current == LED_4) return LED_5;
	else if (current == LED_5) return LED_6;
	else if (current == LED_6) return LED_7;
	else if (current == LED_7) return LED_8;
	else if (current == LED_8) return LED_9;
	else return LED_0;
}

/*
 *	decrement the value on a display by one
 */
leddigit_t displayDecr() {
	// led display port as input
	DISPLAY_DIGIT_DDR = 0x00;
	DISPLAY_DIGIT_PORT = 0x00;

	// get the current displayed value
	leddigit_t current = 0x00;
	if (PIND & (1<<PD0)) current |= 0x01;
	if (PIND & (1<<PD1)) current |= 0x02;
	if (PIND & (1<<PD2)) current |= 0x04;
	if (PIND & (1<<PD3)) current |= 0x08;
	if (PIND & (1<<PD4)) current |= 0x10;
	if (PIND & (1<<PD5)) current |= 0x20;
	if (PIND & (1<<PD6)) current |= 0x40;
	if (PIND & (1<<PD7)) current |= 0x80;

	// set back port as output
	DISPLAY_DIGIT_DDR = 0xff;
	DISPLAY_DIGIT_PORT = 0xff;

	if (current == LED_0) return LED_9;
	else if (current == LED_1) return LED_0;
	else if (current == LED_2) return LED_1;
	else if (current == LED_3) return LED_2;
	else if (current == LED_4) return LED_3;
	else if (current == LED_5) return LED_4;
	else if (current == LED_6) return LED_5;
	else if (current == LED_7) return LED_6;
	else if (current == LED_8) return LED_7;
	else if (current == LED_9) return LED_8;
	else return LED_0;
}
