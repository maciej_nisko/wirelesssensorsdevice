/*
 * led_digit.h
 *
 *  Created on: 15 lip 2014
 *      Author: maciej
 */

/*
 *  This is a header for management the double 7-segment digit display with common anode
 *
 * 		 ___a__
 * 		|      |
 * 		|f     |b
 * 		|___g__|
 * 		|      |
 * 		|e     |c  _
 * 		|___d__|  |_| dp1
 *
 *
 * 	it is connected to the mcu to PORTD as follows:
 * 	a - PD0
 * 	b - PD1
 * 	c - PD2
 * 	d - PD3
 * 	e - PD4
 * 	f - PD5
 * 	g - PD6
 * 	Vcc - PD7
 *
 */

#ifndef LED_DIGIT_H_
#define LED_DIGIT_H_

#include <avr/io.h>


#define DISPLAY_DIGIT_PORT	PORTD
#define DISPLAY_DIGIT_DDR	DDRD


typedef enum {
	LED_0 = 0b11000000,
	LED_1 = 0b11111001,
	LED_2 = 0b10100100,
	LED_3 = 0b10110000,
	LED_4 = 0b10011001,
	LED_5 = 0b10010010,
	LED_6 = 0b10000010,
	LED_7 = 0b11111000,
	LED_8 = 0b10000000,
	LED_9 = 0b10010000
} leddigit_t;


/////////////////////////////////////////////////
// useful functions:

// display a digit
void displayPrintDigit(leddigit_t digit);
void displayOn();
void displayOff();
leddigit_t displayIncr();
leddigit_t displayDecr();


#endif /* LED_DIGIT_H_ */
