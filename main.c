/*
 * main1.c
 *
 *  Created on: 22 lip 2014
 *      Author: maciej
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

#include "hardware_conf.h"
#include "stdio.h"


/*
 * uncomment this line to enable checking if the temperature
 * measurement is correct
 * */
// #define TESTING_TEMP

/*
 * uncomment this line to enable checking if the fsm
 * is working properly
 * */
#define TESTING_FSM


int main(void) {

#ifdef TESTING_TEMP
	char printbuff[100];
#endif

	/* initialize lcd */
	LCD_Initalize();
	LCD_Clear();
	DisplayMainScreen("time", "temp", "light", "humid");

	/* set ds18b20 resolution - 12bit by default */
	OW_SetResolution(OW_RES_10_BIT);

	/*****************************************************/
	/*****************************************************/

	state_t cur_state = ST_DISPLAY;
	instance_data_t data;

	PB_InitButtons();

	/*****************************************************/
	/*****************************************************/

	while(1)
	{

#ifdef TESTING_FSM
		cur_state = RunState(cur_state, &data);
#endif

#ifdef TESTING_TEMP
		OW_GetTemp(printbuff);

		LCD_GoTo(0, 0);
		LCD_WriteText("Temperatura:");
		LCD_GoTo(0, 1);
		LCD_WriteText(printbuff);

		_delay_ms(1000);
#endif
	}

	return 0;
}

