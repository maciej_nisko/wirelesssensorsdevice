/*
 * 1wire.c
 *
 *  Created on: 23 lip 2014
 *      Author: maciej
 *
 *  NOTE:
 *  All the functions written here are based on ATMEL application
 *  note: AVR318.
 */

#include "one_wire.h"
#include "HD44780.h"
#include <util/delay.h>
#include <avr/interrupt.h>
#include "stdlib.h"
#include "stdio.h"


/////////////////////////////////////////////////
// GENERATE RESET PULSE
uint8_t OW_ResetPulse(void) {
	uint8_t i = 1;

	cli();				// disable global interrupts

	OW_DIR_OUT;
	OW_OUT_LOW;			// drive bus low
	_delay_us(480);

	OW_DIR_IN;
	_delay_us(100);

	if( (OW_PIN & (1 << OW_DQ)) == 0 )	// read bus state
		i = 0;							// 0 - OK, 1 - error

	_delay_us(410);
	sei();				// enable global interrupts

	return i;
}


/////////////////////////////////////////////////
// READ BIT FROM SLAVE
uint8_t OW_ReadBit(void) {
	uint8_t bit = 0;

	cli();					// disable global interrupts
	OW_DIR_OUT;
	OW_OUT_LOW;				// drive bus low
	_delay_us(6);			// wait 6us

	OW_DIR_IN;				// release bus
	_delay_us(9);			// wait 9us

	if(OW_PIN & (1 << OW_DQ)) {	// read bus state
		bit = 1;
	}

	_delay_us(55);			// wait 55us
	sei();					// enable global interrupts

	return bit;
}


/////////////////////////////////////////////////
// READ BIT AND WRITE IT DIRECTLY TO LCD
uint8_t OW_ReadWriteBit(void) {
	uint8_t bit = 0;

	cli();					// disable global interrupts
	OW_DIR_OUT;
	OW_OUT_LOW;				// drive bus low
	_delay_us(6);			// wait 6us

	OW_DIR_IN;				// release bus
	_delay_us(9);			// wait 9us

	if(OW_PIN & (1 << OW_DQ)) {	// read bus state
		bit = 1;
		LCD_WriteText("1");
	}

	LCD_WriteText("0");
	_delay_us(55);			// wait 55us
	sei();					// enable global interrupts

	char c[1];
	itoa(bit, c, 10);
	LCD_WriteText(c);
	return bit;
}


/////////////////////////////////////////////////
// WRITE BIT TO SLAVE
void OW_WriteBit(uint8_t bitToWrite) {
	cli();					// disable global interrupts
	OW_DIR_OUT;
	OW_OUT_LOW;				// drive bus low

	if(bitToWrite) {		// write "1"
		_delay_us(6);		// wait 6us
		OW_DIR_IN;			// release bus
		_delay_us(64);		// wait 64us
	}
	else {					// write "0"
		_delay_us(60);		// wait 60us
		OW_DIR_IN;			// release bus
		_delay_us(10);		// wait 10us
	}

	OW_DIR_IN;				// release bus
	sei();					// enable global interrupts
}


/////////////////////////////////////////////////
// READ ONE BYTE
uint8_t OW_ReadByte(void) {
	uint8_t i = 8, n = 0;
	while(i--) {
		n >>= 1;
		n |= (OW_ReadBit() << 7);
	}
	return n;
}


/////////////////////////////////////////////////
// READ ONE BYT AND WRITE IT DIRECTLY TO LCD
uint8_t OW_ReadWriteByte(void) {
	uint8_t i = 8, n = 0;
	while(i--) {
		n >>= 1;
		n |= (OW_ReadWriteBit() << 7);
	}
	return n;
}


/////////////////////////////////////////////////
// WRITE ONE BYTE
void OW_WriteByte(OW_Command_t command) {
	uint8_t i = 8;
	while(i--) {
		OW_WriteBit(command & 1);
		command >>= 1;
	}
}


/////////////////////////////////////////////////
// GET TEMPERATURE FROM SENSOR
void OW_GetTemp(char *buffer) {
	uint8_t temperature[2];
	int8_t digit;
	uint16_t decimal;

	OW_ResetPulse();
	OW_WriteByte(OW_COMM_SKIP_ROM);			// skip ROM
	OW_WriteByte(OW_COMM_CONVERT_T); 		// start temperature conversion

	while(!OW_ReadByte());			// wait until conversion is complete

	OW_ResetPulse();
	OW_WriteByte(OW_COMM_SKIP_ROM);			// skip ROM
	OW_WriteByte(OW_COMM_READ_SCRATCHPAD);	//read scratchpad

	// read first two bytes from scratchpad
	temperature[0] = OW_ReadByte();
	temperature[1] = OW_ReadByte();
	OW_ResetPulse();

	// store temperature integer digits
	digit = temperature[0]>>4;
	digit |= (temperature[1] & 0x07)<<4;

	// store temperature decimal digits
	decimal = temperature[0] & 0x0f;
	decimal *= 625;

	// format temperature into string
	sprintf(buffer, "%d.%04u C", digit, decimal);
}


/////////////////////////////////////////////////
// SET CONVERSION RESOLUTION (12BIT DEFAULT)
void OW_SetResolution(OW_Resolution_t res) {
	OW_ResetPulse();
	OW_WriteByte(OW_COMM_SKIP_ROM);
	OW_WriteByte(OW_COMM_WRITE_SCRATCHPAD);
	OW_WriteByte(0xff);		// T_H register - do not want to use this
	OW_WriteByte(0xff);		// T_L register - do not want to use this
	OW_WriteByte(res);		// CONFIG register
	OW_ResetPulse();
}


