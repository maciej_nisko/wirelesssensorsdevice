/*
 * 1wire.h
 *
 *  Created on: 23 lip 2014
 *      Author: maciej
 */

#ifndef ONE_WIRE_H_
#define ONE_WIRE_H_

#include <avr/io.h>


/////////////////////////////////////////////////
// pin configuration and operations
#define OW_PORT			PORTC
#define OW_DDR			DDRC
#define OW_PIN			PINC
#define OW_DQ			0
#define	OW_DIR_IN		OW_DDR &= ~(1 << OW_DQ)
#define OW_DIR_OUT		OW_DDR |= (1 << OW_DQ)
#define OW_OUT_LOW		OW_PORT &= ~(1 << OW_DQ)
#define OW_OUT_HIGH		OW_PORT |= (1 << OW_DQ)


/////////////////////////////////////////////////
// ds18b20 commands (not all here)
typedef enum {
	OW_COMM_CONVERT_T =	0x44,
	OW_COMM_READ_ROM = 0x33,
	OW_COMM_MATCH_ROM = 0x55,
	OW_COMM_SKIP_ROM = 0xCC,
	OW_COMM_WRITE_SCRATCHPAD = 0x4E,
	OW_COMM_READ_SCRATCHPAD = 0xBE,
	OW_COMM_COPY_SCRATCHPAD = 0x48,
	OW_COMM_ALARM_SEARCH = 0xEC
} OW_Command_t;


/////////////////////////////////////////////////
// set resolution - constants
typedef enum {
	OW_RES_12_BIT = 0x7F,
	OW_RES_11_BIT = 0x5F,
	OW_RES_10_BIT = 0x3F,
	OW_RES_9_Bit = 0x1F
} OW_Resolution_t;


/////////////////////////////////////////////////
// declarations of functions
uint8_t OW_ResetPulse(void);
uint8_t OW_ReadBit(void);
uint8_t OW_ReadWriteBit(void);
void OW_WriteBit(uint8_t bitToWrite);
uint8_t OW_ReadByte(void);
uint8_t OW_ReadWriteByte(void);
void OW_WriteByte(uint8_t byteToWrite);
void OW_GetTemp(char *buffer);
void OW_SetResolution(uint8_t res);


#endif /* ONE_WIRE_H_ */
